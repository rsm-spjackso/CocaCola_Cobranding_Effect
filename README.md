# CocaCola_Cobranding_Effect

48-hour Team Marketing Project analysis of effect of Coca-Cola co-branding with fashion brand Kith

Team Members: 

Xinqi Zhang

Heyue Zhang

Shumeng Shi

Sean Jackson



Consists of python jupyter script to scrape twitter data and the output, R script to analyze the data and the output, and the final presentation 