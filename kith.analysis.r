library(dplyr)
library(ggplot2)

x <- as.data.frame(read.csv('data.csv', header = TRUE))
trend <- as.data.frame(read.csv('trends_jun15_present.csv', header = TRUE))

x['X'] <- NULL
x['tweet'] <- NULL
x['id'] <- NULL


x$date <- as.POSIXct(x$date, format = "%Y-%m-%d %T")
x$calendar <- format(x$date, "%Y-%m-%d")

#favs <- group_by(x, calendar) %>% summarise(mean = mean(favs)) 
favs <- group_by(x,calendar) %>% summarise(sum = sum(favs))
#rt <- group_by(x, calendar) %>% summarise(mean = mean(rt))
rt <- group_by(x, calendar) %>% summarise(sum = sum(rt))


output <- merge(x = favs,y= rt, by = 'calendar', all = TRUE)
names(output) <- c("calendar", "favs", "retweets")

ggplot(data = output, aes(x = as.Date(calendar), y = favs))+  
  geom_line( color = 'blue1', size = 2) 
#  geom_line(y = output$retweets, color = output$retweets, size = 2)


print('done')